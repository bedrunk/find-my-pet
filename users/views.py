from django.shortcuts import get_object_or_404
from django.shortcuts import render
from users.models import CustomUser

from publications.models import Medals, ForAdoption, Publication

# django rest framework
from rest_framework.viewsets import ModelViewSet
from users.serializers import UserSerializer


class CustomUserViewSet(ModelViewSet):
    queryset = CustomUser.objects.all()
    serializer_class = UserSerializer

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())

        # Perform the lookup filtering.
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field

        filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
        obj = get_object_or_404(queryset, **filter_kwargs)

        """ Logica para crear medallas """
        get_medals = Medals.objects.filter(user_id=obj.id)
        my_medals = []

        for i in range(len(get_medals)):
            my_medals.append(get_medals[i].score)

        nobbie = False
        if len(my_medals) == 1:
            nobbie = my_medals[0]

        love_pets = False
        if len(my_medals) == 2:
            love_pets = my_medals[1]

        lomitos_friend = False
        if len(my_medals) == 3:
            lomitos_friend = my_medals[2]

        animalist = False
        if len(my_medals) == 4:
            animalist = my_medals[3]

        master_of_the_puppies = False
        if len(my_medals) == 5:
            master_of_the_puppies = my_medals[4]

        if get_medals.exists() is False:
            """ Si no existe ninguna medalla, crea una cuando tu score sea igual a 2 """
            if obj.score == 2:
                """ Crear primera medalla """
                Medals.objects.create(user_id=obj, name="Nobbie",
                                      icon="https://gitlab.com/bedrunk/findmy.pet-back/wikis/uploads/163f0c1c6acdaa8f0985b02a34507459/noobie.png", score=1)
        else:
            """
                Si tienes alguna medalla verifica el score y crea la medalla que corresponda. 
            """
            if nobbie != 2 and obj.score == 2:
                """ Tienes 2 puntos de experiencia """
                Medals.objects.create(user_id=obj, name="Nobbie",
                                      icon="https://gitlab.com/bedrunk/findmy.pet-back/wikis/uploads/163f0c1c6acdaa8f0985b02a34507459/noobie.png", score=1)
            elif love_pets != 10 and obj.score == 10:
                Medals.objects.create(user_id=obj, name="Love pets",
                                      icon="https://gitlab.com/bedrunk/findmy.pet-back/wikis/uploads/ec1145122bcd5bb9fe4f8a94f1f12473/love-pets.png", score=10)

            elif lomitos_friend != 25 and obj.score == 25:
                Medals.objects.create(user_id=obj, name="Lomitos friend",
                                      icon="https://gitlab.com/bedrunk/findmy.pet-back/wikis/uploads/0a0ac8194c3906c2335dac92b43d4fa3/lomitos-friend.png", score=25)

            elif animalist != 45 and obj.score == 45:
                Medals.objects.create(user_id=obj, name="Animalist",
                                      icon="https://gitlab.com/bedrunk/findmy.pet-back/wikis/uploads/28659c887c953d538b38e317e556c8d2/animalist.png", score=45)

            elif master_of_the_puppies != 70 and obj.score == 70:
                Medals.objects.create(user_id=obj, name="Master of the puppies",
                                      icon="https://gitlab.com/bedrunk/findmy.pet-back/wikis/uploads/591720b6da1901dfb9e21cb51cb1a781/master-of-the-puppies.png", score=70)
            else:
                pass

        # May raise a permission denied
        self.check_object_permissions(self.request, obj)

        return obj
