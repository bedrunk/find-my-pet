from django.contrib.auth.models import AbstractUser
from django.db import models


class CustomUser(AbstractUser):
    name = models.CharField(blank=True, max_length=255)
    avatar = models.ImageField(
        upload_to='users/avatar', null=True, blank=True)
    bio = models.TextField(blank=True, null=True)
    score = models.PositiveIntegerField(null=True, blank=True, default=1)

    def __str__(self):
        return self.username
