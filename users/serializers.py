from rest_framework import serializers
from users.models import CustomUser
from publications.serializers import MedalsSerializer


class UserSerializer(serializers.ModelSerializer):
    medals = MedalsSerializer(many=True, read_only=True)

    class Meta:
        model = CustomUser
        fields = [
            'id',
            'username',
            'name',
            'email',
            'avatar',
            'bio',
            'score',
            'medals',
            'url'
        ]
