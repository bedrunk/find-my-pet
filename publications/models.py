from django.db import models
from django.conf import settings

from users.models import CustomUser


class Publication(models.Model):

    TYNY = 0
    SMALL = 1
    MEDIUM = 2
    BIG = 3
    VERY_BIG = 4

    SIZE_CHOICES = (
        (TYNY, 'Diminuto'),
        (SMALL, 'Pequeño'),
        (MEDIUM, 'Mediano'),
        (BIG, 'Grande'),
        (VERY_BIG, 'Enorme'),
    )

    NEGRO = 0
    BLANCO = 1
    CAFE = 2
    MIEL = 3
    MANCHADO = 4

    COLOR_CHOICES = (
        (NEGRO, 'Negro'),
        (BLANCO, 'Blanco'),
        (CAFE, 'Cafe'),
        (MIEL, 'Miel'),
        (MANCHADO, 'Manchado'),
    )

    MALE = 0
    FEMALE = 1
    UNDEFINED = 2

    GENDER_CHOICES = (
        (MALE, 'Masculino'),
        (FEMALE, 'Femenino'),
        (UNDEFINED, 'No identificado'),
    )

    LABRADOR = 0
    PUG = 1
    CHIHUAHUA = 2
    DOBERMAN = 3
    BULLDOG = 4
    ZCHNAUZER = 5
    CRUZA = 6
    OTRO = 7

    BREED_CHOICES = (
        (LABRADOR, 'Labrador'),
        (PUG, 'Pug'),
        (CHIHUAHUA, 'Chihuahua'),
        (DOBERMAN, 'Doberman'),
        (BULLDOG, 'Bulldog'),
        (ZCHNAUZER, 'Zchnauzer'),
        (CRUZA, 'Cruza'),
        (OTRO, 'Otro'),
    )

    WANTED = 0
    SIGHTING = 1

    REPORT_TYPE = (
        (WANTED, 'Se busca'),
        (SIGHTING, 'Avistado'),
    )

    created = models.DateField(auto_now_add=True)
    title = models.CharField(max_length=250)
    description = models.TextField()
    size = models.PositiveSmallIntegerField(
        choices=SIZE_CHOICES, default=SMALL)
    color = models.PositiveSmallIntegerField(
        choices=COLOR_CHOICES, default=NEGRO)
    gender = models.PositiveSmallIntegerField(
        choices=GENDER_CHOICES, default=UNDEFINED)
    breed = models.PositiveSmallIntegerField(
        choices=BREED_CHOICES, default=OTRO)
    location = models.CharField(max_length=250)
    report_type = models.PositiveSmallIntegerField(
        choices=REPORT_TYPE, default=SIGHTING)
    snapshot = models.ImageField(
        upload_to='snapshot/pets', null=True, blank=True)
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.title


class ForAdoption(models.Model):

    created = models.DateField(auto_now_add=True)
    tutor = models.ForeignKey(
        settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)
    title = models.CharField(max_length=250)
    image = models.ImageField(
        upload_to='for_adoption/pets', null=True, blank=True)
    description = models.TextField()
    size = models.PositiveSmallIntegerField()
    color = models.PositiveSmallIntegerField()
    gender = models.PositiveSmallIntegerField()
    breed = models.PositiveSmallIntegerField()
    # score = models.PositiveIntegerField()


class Medals(models.Model):

    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=250, null=True, blank=True)
    icon = models.URLField(null=True, blank=True)
    score = models.PositiveIntegerField(null=True, blank=True)
    user_id = models.ForeignKey(
        CustomUser, related_name='medals', on_delete=models.CASCADE, null=True, blank=True)
