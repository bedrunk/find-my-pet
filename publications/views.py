from django.shortcuts import render
from publications.models import Publication, ForAdoption, Medals

from users.models import CustomUser


# django rest framework
from rest_framework import status
from rest_framework.response import Response

from rest_framework.viewsets import ModelViewSet
from publications.serializers import (
    PublicationSerializer, ForAdoptionSerializer, MedalsSerializer)


class PublicationViewSet(ModelViewSet):
    queryset = Publication.objects.all()
    serializer_class = PublicationSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        get_user = serializer.data
        filter_user = CustomUser.objects.get(id=get_user["author"])
        score = filter_user.score + 1
        CustomUser.objects.filter(id=get_user["author"]).update(score=score)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class ForAdoptionViewSet(ModelViewSet):
    queryset = ForAdoption.objects.all()
    serializer_class = ForAdoptionSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        get_user = serializer.data
        filter_user = CustomUser.objects.get(id=get_user["tutor"])
        score = filter_user.score + 1
        CustomUser.objects.filter(id=get_user["tutor"]).update(score=score)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class MedalsViewSet(ModelViewSet):
    queryset = Medals.objects.all()
    serializer_class = MedalsSerializer
