from django.urls import include, path

from rest_framework.routers import DefaultRouter

from publications import views

from users.views import CustomUserViewSet


router = DefaultRouter()
router.register('user', CustomUserViewSet)
router.register('publication', views.PublicationViewSet)
router.register('for-adoption', views.ForAdoptionViewSet)
router.register('medals', views.MedalsViewSet)

urlpatterns = [
    path('rest-auth/', include('rest_auth.urls')),
    path('rest-auth/registration/', include('rest_auth.registration.urls')),
]


urlpatterns += router.urls
