from rest_framework import serializers
from publications.models import Publication, ForAdoption, Medals


class PublicationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Publication
        fields = [
            'id',
            'created',
            'title',
            'description',
            'size',
            'color',
            'gender',
            'breed',
            'location',
            'report_type',
            'snapshot',
            'author',
            'url'
        ]


class ForAdoptionSerializer(serializers.ModelSerializer):

    class Meta:
        model = ForAdoption
        fields = [
            'id',
            'tutor',
            'title',
            'image',
            'description',
            'size',
            'color',
            'gender',
            'breed',
            'url'
        ]


class MedalsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Medals
        fields = [
            'id',
            'user_id',
            'name',
            'icon',
            'score',
            'url'
        ]
